"""Implements wrapper for axapta bussiness connector."""

import pathlib
from msl.loadlib import LoadLibrary

DLL_PATH = pathlib.Path(
    __file__).parent / 'Microsoft.Dynamics.BusinessConnectorNet.dll'


def test_msl_connector():
    """Get Axapta object via msl-loadlib package."""
    connectorLL = LoadLibrary(DLL_PATH, 'net')
    Microsoft = getattr(connectorLL.lib,
                        'Microsoft.Dynamics.BusinessConnectorNet')
    Axapta = Microsoft.Dynamics.BusinessConnectorNet.Axapta
    return Axapta